var WxParse = require('./wxParse/wxParse.js')
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    detail: String
  },

  /**
   * 组件的初始数据
   */
  data: {
  },
  lifetimes: {
    created: function () {
      WxParse.emojisInit('[]', "./wxParse/emojis/", {
        "00": "00.gif",
        "01": "01.gif",
        "02": "02.gif",
        "03": "03.gif",
        "04": "04.gif",
        "05": "05.gif",
        "06": "06.gif",
        "07": "07.gif",
        "08": "08.gif",
        "09": "09.gif",
        "09": "09.gif",
        "10": "10.gif",
        "11": "11.gif",
        "12": "12.gif",
        "13": "13.gif",
        "14": "14.gif",
        "15": "15.gif",
        "16": "16.gif",
        "17": "17.gif",
        "18": "18.gif",
        "19": "19.gif",
      })
    }
  },
  observers: {
    
    'detail': function (detail) {
      const vm = this
      var article = detail

      WxParse.wxParse('article', 'html', article, vm, 0)
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {

    /**
     * 点击富文本中的超链接。注意:
     * 只有微信小程序配置的合法业务域名允许跳转！
     */
    wxParseTagATap: function (e) {
      var href = e.currentTarget.dataset.src
      console.log(href)
      wx.redirectTo({
        url: href
      })
    }
  }
})
