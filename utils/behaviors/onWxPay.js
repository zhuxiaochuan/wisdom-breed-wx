const util = require('../util')
const api = require('../../config/api.js')
const app = getApp()
const payStatus = {
  //待支付
  tobepaid: 1,
  //支付中
  inpayment: 2,
  //支付成功
  success: 3,
  //支付失败
  error: 4
}
const outTradeNo = ''
module.exports = Behavior({
  
  //数据
  data:{
    //订单号  必填
    orderNo: '',

    //下单时间 必填
    createTime: '',

    //商品名称   必填
    goodsName: '',

    //订单金额(单位为分) 必填
    amount:0
  },

  methods: {
    getPayParam(callback) {
      //统一下单
      let behavior = this
      let res = {
        status: '300',
        message: '统一下单失败',
        data: null
      }
      app.getUserInfo(function (userInfo) {
        util.wxRequest({
          url: api.WxApiRoot + 'wx/pay/unifiedOrder.html',
          method: 'post',
          data: {
            openid: userInfo.openId,
            orderNo: behavior.data.orderNo,
            goodsName: behavior.data.goodsName,
            amount: behavior.data.amount
          },
          success: function (obj) {
            let ret = obj.data
            if ('200' === ret.status) {
              //用于调起微信支付的数据，通过统一下单后服务端返回
              let payParam = {
                signType: 'MD5',
                timeStamp: ret.data.timeStamp,
                nonceStr: ret.data.nonceStr,
                package: ret.data.packageInfo,
                //微信签名
                paySign: ret.data.paySign,
                outTradeNo: ret.data.outTradeNo
              }

              res.message = 'SUCCESS'
              res.status='200'
              res.data = payParam
              typeof callback == "function" && callback(res)
            } else {
              res.message = ret.message
              typeof callback == "function" && callback(res)
            }
          },
          fail: function () {
            res.message = '服务器连接失败'
            typeof callback == "function" && callback(res)
          }
        })
      })// end of getUserInfo 
    },

    //调起微信支付
    onWxPay(callback) {
      let behavior = this
      let pageResult = {
        status: '300',
        message: '支付失败',
        data: null
      }

      behavior.getPayParam(function (res) {
    	  console.log(JSON.stringify(res))
        if ('200' === res.status) {
          //获取支付参数成功
          let payParam = res.data
          console.log('------------console.log(res.data)-------------')
          console.log(res.data)

          wx.requestPayment({
            timeStamp: payParam.timeStamp,
            nonceStr: payParam.nonceStr,
            package: payParam.package,
            signType: 'MD5',
            paySign: payParam.paySign,
            outTradeNo:payParam.outTradeNo,          
            success(res) {
              //支付成功,调用同步订单处理
              console.log('支付成功,调用同步订单处理')
              util.wxRequest({
                url: api.WxApiRoot + '/wx/pay/paySuccess.html',
                data: {
                  outTradeNo: payParam.outTradeNo,
                  payResult: payStatus.success
                },
                success: function (obj) {
                  let ret = obj.data
                  console.log("支付返回" + JSON.stringify(ret) )
                  pageResult = {
                    status: ret.status,
                    message: ret.message,
                    data: ret.data
                  }
                  typeof callback == "function" && callback(pageResult)
                },
                fail: function () {
                  pageResult.message = '服务器连接失败'
                  typeof callback == "function" && callback(pageResult)
                }
              })
            },
            fail(res) {
              //支付失败
              pageResult.message = '支付失败'
              console.log('支付失败')
              pageResult.data = res
              typeof callback == "function" && callback(pageResult)
            }
          })
        } else {
          //统一下单失败
          pageResult.message = res.message
          typeof callback == "function" && callback(pageResult)
        }        
      })
    }// end of onWxPay 
  }    
})
