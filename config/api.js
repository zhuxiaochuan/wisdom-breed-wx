// 本机
//const Server = 'localhost:8080'
// 线上
const Server = 'otapi.yiluaiot.com'

const HttpProtocol = 'https'

// ------------------------------------------------------------------------------------------------------
const WxApiRoot = HttpProtocol+'://'+Server+'/wx/'

module.exports = {
  // root path
  WxApiRoot: WxApiRoot,

  // 登录检查
  AuthLoginCheck: WxApiRoot + 'auth/loginCheck',
   
  // login
  AuthLogin: WxApiRoot + 'auth/login',

  // 企业列表
  EnterpriseList: WxApiRoot + 'index/enterpriseList',

  // 场地列表
  PlaceList: WxApiRoot + 'index/placeList',

  // 首页数据
  IndexData: WxApiRoot + 'index/indexData',

  // 数量盘点详情
  IndexDetail: WxApiRoot + 'index/indexDetail',

  // 分娩盘点详情
  DeliverDetail: WxApiRoot + 'index/deliverDetail',

  // 查看图片
  ImageDetail: WxApiRoot + 'index/imageDetail',

  // 盘点报表
  RevisionData: WxApiRoot + 'revision/data',

  // 分娩报表
  RevisionDeliverData: WxApiRoot + 'revision/deliverData',

  // 直播-----------------------------------------------------
  // 栋舍列表
  VideoDormList: WxApiRoot + 'video/dormList',

  // 摄像头列表
  VideoCameraList: WxApiRoot + 'video/cameraList',

  // 直播地址
  VideoLiveUrl: WxApiRoot + 'video/playurl',

  // 退出登录
  AuthLogout: WxApiRoot + 'auth/logout',

  // 修改密码
  ModifyPass: WxApiRoot + 'my/updatePassword'
}
