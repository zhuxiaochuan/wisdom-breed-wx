// pages/pdDetail/pdDetail.js
const api = require('../../config/api.js')
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    eId: '',
    pId: '',
    date: '',
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    
  },

  goImgList(event){
    const vm = this
    let did = event.currentTarget.dataset.did
    wx.navigateTo({
      url: '/pages/imgList/imgList?eId='+vm.data.eId+'&pId='+vm.data.pId+'&date='+vm.data.date+'&dId='+did
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    const vm = this
    let pages = getCurrentPages();
    let currentPage = pages[pages.length-1];
    let options = currentPage.options
    vm.setData({
      eId: options.eId,
      pId: options.pId,
      date: options.date
    })
    vm.getData()
  },
  getData(){
    const vm = this
    util.wxRequest({
      url: api.IndexDetail,
      method: 'get',
      header:{
        'content-type': 'application/json'
      },
      data: {
        enterpriseId: vm.data.eId,
        placeId: vm.data.pId,
        date: vm.data.date
      },
      success: function (res) {
        let row = res.data
        vm.setData({
          list: row.data.list
        })
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})