const api = require('../../config/api.js')
const util = require('../../utils/util.js')
let vCtx = null
const app = getApp()
Page({
  data: {
    option1: [],
    value1: 0,
    activeKey: 0,
    list: [],
    vList: [],
    show: false,
    url: 'https://1500005692.vod2.myqcloud.com/43843706vodtranscq1500005692/62656d94387702300542496289/v.f100240.m3u8'
  },
  onShow(){
    const vm = this
    vCtx = wx.createVideoContext('myVideo', vm)
    vm.getOpt2()
  },
  onClose() {
    const vm = this
    vCtx.pause()
    vm.setData({ show: false });
  },
  imgClick(e){
    const vm = this
    let accessType = e.currentTarget.dataset.type
    if(1 !== accessType){
      wx.showToast({
        title: '录像机网络接入类型不支持直播',
        icon: 'none',
        duration: 3000
      })
      return
    }
    let id = e.currentTarget.dataset.id
    util.wxRequest({
      url: api.VideoLiveUrl + '/' + id,
      method: 'get',
      success: function (res) {
        let row = res.data
        vm.setData({
          show: true,
          url: row.data
        });
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },
  getVlist(id){
    const vm = this
    util.wxRequest({
      url: api.VideoCameraList,
      method: 'get',
      header:{
        'content-type': 'application/json'
      },
      data: {
        dormId: id
      },
      success: function (res) {
        let row = res.data
        vm.setData({
          vList: row.data
        })
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },
  listChange(e){
    const vm = this
    let id = vm.data.list[e.detail].id
    vm.getVlist(id)
  },
  getList(){
    const vm = this
    util.wxRequest({
      url: api.VideoDormList,
      method: 'get',
      header:{
        'content-type': 'application/json'
      },
      data: {
        placeId: vm.data.value1
      },
      success: function (res) {
        let row = res.data
        vm.setData({
          list: row.data
        })
        if(!util.isEmpty(row.data)){
          vm.getVlist(row.data[0].id)
        }
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },
  // 场地选择
  change1({detail}){
    const vm = this
    vm.setData({
      value1: detail
    })
    vm.getList()
  },
  // 场地列表
  getOpt2(){
    const vm = this
    util.wxRequest({
      url: api.PlaceList,
      method: 'get',
      header:{
        'content-type': 'application/json'
      },
      data: {
        enterpriseId: util.getStorageSync('enterpriseId')
      },
      success: function (res) {
        let row = res.data
        let option2 = []
        row.data.placeList.forEach(item => {
          option2.push({
            text: item.placeName,
            value: item.placeId
          })
        })
        vm.setData({
          option1: option2,
          value1: util.getStorageSync('placeId')
        })
        vm.getList()
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },
})