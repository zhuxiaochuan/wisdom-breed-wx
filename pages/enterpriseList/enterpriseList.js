// pages/enterpriseList/enterpriseList.js
const api = require('../../config/api.js')
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    enterpriseId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    const vm = this
    vm.setData({
      enterpriseId: wx.getStorageSync('enterpriseId')
    })
    vm.getList()
  },
  goHome(event){
    const vm = this
    let eid = event.currentTarget.dataset.eid
    let eName =  event.currentTarget.dataset.ename
    wx.setStorage({
      key:"enterpriseId",
      data: eid
    })
    console.log(event)
    wx.setStorageSync('enterpriseName', eName)
    wx.navigateTo({
      url: '/pages/fieldList/fieldList',
    })
  },
  getList(){
    const vm = this
    util.wxRequest({
      url: api.EnterpriseList,
      method: 'get',
      header:{},
      data: {},
      success: function (res) {
        let row = res.data
        vm.setData({
          list: row.data.enterpriseList
        })
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})