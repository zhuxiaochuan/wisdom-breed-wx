const api = require('../../config/api.js')
const util = require('../../utils/util.js')

const app = getApp()
Page({
  data: {
    option1: [],
    value1: 0,
    TabCur: 0,
    scrollLeft:0,
    arr: [
      '周',
      '月',
      '季',
      '半年',
      '年',
    ],
    list: []
  },
  onShow(){
    const vm = this
    vm.getList()
    vm.getOpt2()
  },
  getList(){
    const vm = this
    util.wxRequest({
      url: api.RevisionDeliverData,
      method: 'get',
      header:{
        'content-type': 'application/json'
      },
      data: {
        enterpriseId: util.getStorageSync('enterpriseId'),
        placeId: vm.data.value1,
        queryType: vm.data.TabCur + 1
      },
      success: function (res) {
        let row = res.data
        row.data.deliverSheet.forEach(item => {
          item.day1 = item.day.split(' ')[0]
        })
        vm.setData({
          list: row.data.deliverSheet
        })
        console.log(vm.data.list)
      }
    })
  },
  // 场地选择
  change2({detail}){
    const vm = this
    vm.setData({
      value1: detail
    })
    vm.getList()
  },
  // 场地列表
  getOpt2(){
    const vm = this
    util.wxRequest({
      url: api.PlaceList,
      method: 'get',
      header:{
        'content-type': 'application/json'
      },
      data: {
        enterpriseId: util.getStorageSync('enterpriseId')
      },
      success: function (res) {
        let row = res.data
        let option2 = []
        row.data.placeList.forEach(item => {
          option2.push({
            text: item.placeName,
            value: item.placeId
          })
        })
        vm.setData({
          option1: option2,
          value1: util.getStorageSync('placeId')
        })
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },
  tabSelect(e) {
    const vm = this
    vm.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id-1)*60
    })
    vm.getList()
  }
})