// pages/fieldList/fieldList.js
const api = require('../../config/api.js')
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    placeId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    const vm = this
    vm.setData({
      placeId: wx.getStorageSync('placeId')
    })
    vm.getList()
  },
  goHome(event){
    const vm = this
    let eid = event.currentTarget.dataset.eid
    let eName = event.currentTarget.dataset.ename
    console.log('SET PLACE ID',eid)
    wx.setStorageSync('placeId', eid)
    wx.setStorageSync('placeName', eName)
    wx.switchTab({
      url: '/pages/home/home',
    })
  },
  getList(){
    const vm = this
    let eid = util.getStorageSync('enterpriseId')
    util.wxRequest({
      url: api.PlaceList,
      method: 'get',
      header:{},
      data: {
        enterpriseId: eid
      },
      success: function (res) {
        let row = res.data
        vm.setData({
          list: row.data.placeList
        })
        if(row.data.placeList && row.data.placeList.length > 0){
          wx.setStorageSync('placeId', row.data.placeList[0].placeId)
          wx.setStorageSync('placeName', row.data.placeList[0].placeName)
        }
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})