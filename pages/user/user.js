var util = require('../../utils/util.js')
var User = require('../../utils/user.js')
var api = require('../../config/api.js')
import WxValidate from '../../utils/wxValidate/WxValidate.js'

var app = getApp()
Page({
  data: {
    //客服电话
    phonenum: '15210923206',
    app: app,
    userInfo: {
      realName: '点击登录'
    },
    hasLogin: false,
    enterpriseName: '',
    placeName: ''
  },
  computed: {
    
  },
  
  onShow(){
    wx.hideLoading()
    let page = this

    page.setData({
      enterpriseName: wx.getStorageSync('enterpriseName'),
      placeName: wx.getStorageSync('placeName')
    })
    if (typeof page.getTabBar === 'function' &&
      page.getTabBar()) {
      page.getTabBar().setData({
        active: 3
      })
    }

    app.hasLogin(function (_hasLogin) {
      if (_hasLogin) {
        app.getUserInfo(function (userInfo) {
          page.setData({
            userInfo: userInfo,
            hasLogin: true
          })
        })
      }
    })
  },

  onCancel(){
    const page = this
    page.setData({
      dialogShow: false
    })
  },

  tolinkUrl: function (e) {
    let linkUrl = e.currentTarget.dataset.link
    wx.navigateTo({
      url: linkUrl
    })
  },

  // 拨打电话
  makePhoneCall(e){
    const phonenum = e.currentTarget.dataset.phonenum
    wx.makePhoneCall({
      phoneNumber: phonenum
    })
  },

  goVideo(){
    const vm = this
    wx.navigateTo({
      url: '/pages/videos/videos',
    })
  },

  goLogin() {
    if (!this.data.hasLogin) {
      wx.navigateTo({
        url: '/pages/auth/login/login'
      })
    }
  },
  exitLogin: function () {
    wx.showModal({
      title: '',
      confirmColor: '#2962C8',
      content: '退出登录？',
      success: function (res) {
        if (!res.confirm) {
          return
        }
        util.request(api.AuthLogout, {}, 'GET').then((logoutRes) => {
          User.updateStorageUserInfo(null)
          wx.reLaunch({
            url: '/pages/home/home'
          })
        });
      }
    })

  },
  
  // 选择企业
  myPlace1(){
    const page = this
    wx.navigateTo({
      url: '/pages/enterpriseList/enterpriseList',
    })
  },
  // 选择场地
  myPlace2(){
    const page = this
    wx.navigateTo({
      url: '/pages/fieldList/fieldList',
    })
  },

  showPassword(){
    const page = this
    
  },

  showEdit(){
    const page = this
    wx.navigateTo({
      url: '/pages/editPass/editPass',
    })
  }
})