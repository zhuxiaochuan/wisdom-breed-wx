var api = require('../../../config/api.js')
var util = require('../../../utils/util.js')
var UserUtil = require('../../../utils/user.js')

var app = getApp()
Page({
  data:{
    tel: null,
    password: null,
    canIUseGetUserProfile: false
  },
  onLoad: function(options) {
    // 页面渲染完成
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  cancelLogin(){
    wx.reLaunch({
      url: '/pages/home/home'
    })
  },
  // 企业列表
  getOpt1(){
    const vm = this
    util.wxRequest({
      url: api.EnterpriseList,
      method: 'get',
      header:{},
      data: {},
      success: function (res) {
        let row = res.data
        wx.setStorageSync('enterpriseId', row.data.enterpriseList[0].enterpriseId)
        wx.setStorageSync('enterpriseName', row.data.enterpriseList[0].enterpriseName)
        // 返回上一级页面
        util.navigatePrev()
      }
    })
  },
  login: function(e) {
    const page = this
    wx.showLoading({
      title: '正在登录...'
    })
    
    util.wxRequest({
      url: api.AuthLogin,
      method: 'post',
      header:{
        'content-type': 'application/json'
      },
      data: {
        tel: page.data.tel,
        password: page.data.password
      },
      success: function (obj) {
        let ret = obj.data
        console.log('ret',ret)

        if(ret.code === 0){
          let user = ret.data.user
          user.token = ret.data.token
          console.log('user',user)
          
          // 更新缓存
          UserUtil.updateStorageUserInfo(user)
          page.getOpt1()
        }else{
          util.showToast(ret.msg)
        }
      },
      complete: () => {
        setTimeout(() => {
          wx.hideLoading()
        },2000)
      }
    })

  }
})