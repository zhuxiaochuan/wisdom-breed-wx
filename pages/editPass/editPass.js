// pages/editPass/editPass.js
const api = require('../../config/api.js')
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: '',
    password: '',
    password2: ''
  },

  okClick(){
    const vm = this
    util.wxRequest({
      url: api.ModifyPass,
      method: 'post',
      header:{
        'Content-Type': 'application/json'
      },
      data: {
        originPassword: vm.data.username,
        newPassword: vm.data.password,
        confirmPassword: vm.data.password2
      },
      success: function (res) {
        let row = res.data
        if(row.code == 0){
          wx.showToast({
            title: '修改密码成功',
            duration:2000,
            mask:true,
            icon:'success', 
            success:function(){
              setTimeout(() => {
                util.navigatePrev()
              },2000)
            }
          })
        }else{
          if(row.msg){
            util.showToast(row.msg)
          }else{
            util.showToast('请求失败')
          }
        }
      },
      complete: () => {
        wx.hideLoading()
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})